// vi: ft=typescript
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { mongoose } from "./db/database";

class App {

    constructor() {
        // create expressjs application
        this.app = express();
        // configure application
        this.config();
        // add routes
        this.routes();
    }

    public app: express.Application;

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(function(error, request, response, next) {
            console.error(error.message);
            response.status(500).send('An error has occured.');
        });
        // test db connection status
        console.log(mongoose.connection.readyState);
    }

    private routes(): void {
        const router = express.Router();

        router.get('/', (req: Request, res: Response) => {
            res.status(200).send({
                message: 'Hello World!'
            })
        });

        router.post('/', (req: Request, res: Response) => {
            const data = req.body;
            res.status(200).send(data);
        });

        this.app.use('/', router);
    }
}

export default new App().app;
