import * as mongoose from "mongoose";

(mongoose as any).Promise = global.Promise;
let MONGODB_URI = "mongodb://"+process.env.DB_USER+":"+process.env.DB_PASS+"@"+process.env.DB_HOST+":"+process.env.DB_PORT+"/"+process.env.DB_NAME;
mongoose.connect(MONGODB_URI);

export { mongoose };
