# This is a sample node api server using expressjs

## create a file .env at the root of the repository,  defining the enviromental variables

## install typescript
```
npm i -g typescript
```

## install the dependencies
```
npm install
```
## build
```
npm run build
```

## start server
```
npm run start
```
